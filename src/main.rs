use anyhow::{Context, Result};
use hyprland::data::Clients;
use hyprland::data::Monitors;
use hyprland::dispatch::DispatchType::*;
use hyprland::dispatch::WindowIdentifier::Address;
use hyprland::dispatch::*;
use hyprland::prelude::*;

#[tokio::main]
async fn main() -> Result<()> {
    // get all clietns
    let clients = Clients::get_async()
        .await
        .context("Failed to get clients")?
        .collect();
    let monitors = Monitors::get_async().await?.collect();
    let mut monitor_width = 0;
    let mut monitor_height = 0;
    for i in monitors.iter() {
        if monitor_width < i.width as i16 {
            monitor_width = i.width as i16;
        }
        if monitor_height < i.height as i16 {
            monitor_height = i.height as i16;
        }
    }

    // close the window if it has an empty title
    for client in clients.iter() {
        if client.title.is_empty() {
            Dispatch::call_async(FocusWindow(Address(client.address.clone())))
                .await
                .context("Failed to focus window")?;

            Dispatch::call_async(KillActiveWindow)
                .await
                .context("Failed to kill active window")?;
            Dispatch::call_async(ResizeActive(Position::Exact(20, 0)))
                .await
                .context("Failed to resize active window")?;
            Dispatch::call_async(MoveActive(Position::Exact(
                monitor_width + 20,
                monitor_height + 20,
            )))
            .await
            .context("Failed to move the active window")?;
        } else if client.title == "RemoteApp Marker Window" {
            Dispatch::call_async(CloseWindow(WindowIdentifier::Address(
                client.address.clone(),
            )))
            .await
            .context("Failed to close RemoteApp Marker window(The weird outer borders)")?;
        }
    }
    Ok(())
}
